from datetime import date, datetime
from flask_login import UserMixin

import bcrypt
from sqlalchemy import Column, Integer, Date, DateTime, Text, ForeignKey, Boolean, Numeric
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from whfc.create_app import db

Base = declarative_base()


# A news article
class Players(Base):
    __tablename__ = 'whfc_players'

    id = Column(Integer, primary_key=True, autoincrement=True)
    first_name = Column(Text, nullable=False)
    last_name = Column(Text, nullable=False)
    status = Column(Integer, nullable=False, default=1)
    img = Column(Boolean, nullable=False, default=0)
    full_name = Column(Text, nullable=True)
    nickname = Column(Text, nullable=True, default='-')
    fav_team = Column(Text, nullable=True, default='-')
    dob = Column(Date, nullable=True)
    nation = Column(Text, nullable=True, default='-')
    position = Column(Text, nullable=True, default='-')

    @property
    def get_age(self):
        today = date.today()
        return today.year - self.dob.year - ((today.month, today.day) < (self.dob.month, self.dob.day))

    @property
    def get_stats(self):
        from whfc.db.api import get_player_stats
        return get_player_stats(self.id)


class PlayerStats(Base):
    __tablename__ = 'player_stats'

    player_id = Column(Integer, primary_key=True)
    games = Column(Integer)
    goals = Column(Integer)
    assists = Column(Integer)
    mom = Column(Integer)
    clean_sheets = Column(Integer)
    new_player_id = Column(Integer, ForeignKey("player.id"))

    @property
    def stats_points(self):
        return self.games + self.goals * 2 + self.assists + self.mom * 2 + self.clean_sheets


class StaffMembers(Base, UserMixin):
    __tablename__ = 'staff_members'

    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(Text, nullable=False)
    password = Column(Text, nullable=False)
    role_type_id = Column(Integer, nullable=False)
    status_id = Column(Integer, nullable=False, default=1)
    date_created = Column(Date, nullable=False, default=datetime.now())
    date_updated = Column(Date, nullable=True)
    role = Column(Text, nullable=False)
    full_name = Column(Text, nullable=False)
    description = Column(Text, nullable=False)
    image = Column(Text, nullable=False)
    url = Column(Text, nullable=False)
    priority = Column(Integer, nullable=False)

    def check_password(self, password):
        if password is None:
            return False
        if self.password is None:
            return False
        return bcrypt.checkpw(password.encode('utf-8'), self.password.encode('utf-8'))

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    @property
    def is_authenticated(self):
        return True

    @property
    def role_id(self):
        if self.role_type_id == 1:
            return "Admin"
        if self.role_type_id == 2:
            return "User"

    @property
    def status(self):
        if self.status_id == 1:
            return "Pending"
        if self.status_id == 2:
            return "Active"
        if self.status_id == 3:
            return "Deleted"
        if self.status_id == 4:
            return "Banned"

    def get_id(self):
        return self.id

    def change_password(self, existing_password, new_password):
        if not self.check_password(existing_password):
            return False
        self.password = bcrypt.hashpw(
                password=new_password.encode('utf-8'),
                salt=bcrypt.gensalt(12)
        )
        return True


class Fixtures(Base):
    __tablename__ = 'fixtures'

    id = Column(Integer, primary_key=True, autoincrement=True)
    date = Column(Date)
    home = Column(Boolean)
    venue = Column(Text)
    type = Column(Text)
    against = Column(Text)
    time = Column(Text)
    url = Column(Text)
    score = Column(Text)
    game = Column(Boolean)
    report = Column(Text)
    album = Column(Text)
    h1 = Column(Text)
    short_desc = Column(Text)


class Player(Base):
    __tablename__ = 'player'

    id = Column(Integer, primary_key=True, autoincrement=True)
    first_name = Column(Text, nullable=False)
    last_name = Column(Text, nullable=False)
    full_name = Column(Text, nullable=True)
    nickname = Column(Text, nullable=True)
    dob = Column(Date, nullable=True)
    nationality = Column(Text, nullable=True)
    position_id = Column(Integer, ForeignKey("position.id"), nullable=True)
    favourite_team = Column(Text, nullable=True)
    status_id = Column(Integer, nullable=False, default=1)
    previous_clubs = Column(Text, nullable=True)
    shirt_number = Column(Integer, nullable=True)

    """ :type list[whfc.db.models.MapPosition] """
    position = relationship('Position')

    money_owed = relationship("PlayerDebit")
    """ :type list[whfc.db.models.PlayerDebit] """
    money_paid = relationship("PlayerCredit")
    """ :type list[whfc.db.models.PlayerCredit] """
    images = relationship("PlayerImage")
    """ :type list[whfc.db.models.PlayerImage] """
    club_registrations = relationship('ClubPlayerRegistration')
    """ :type list[whfc.db.models.ClubPlayerRegistration] """
    goals = relationship('FixtureEventGoal')
    """ :type list[whfc.db.models.FixtureEventGoal] """
    assists = relationship('FixtureEventGoalAssist')
    """ :type list[whfc.db.models.FixtureEventGoalAssist] """

    team_player_registrations = relationship('ClubTeamPlayerRegistration')
    """ :type list[whfc.db.models.ClubTeamPlayerRegistration] """

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    @property
    def monies_owed(self):
        return sum(x.amount for x in self.money_owed)

    @property
    def monies_paid(self):
        return sum(x.amount for x in self.money_paid)

    @property
    def balance(self):
        return self.monies_paid - self.monies_owed

    @property
    def current_club(self):
        for r in self.club_registrations:
            if r.registered_from <= datetime.date(datetime.today()) <= r.registered_to:
                return r.club
        return None

    @property
    def age(self):
        today = date.today()
        return today.year - self.dob.year - ((today.month, today.day) < (self.dob.month, self.dob.day))

    @property
    def stats(self):
        from whfc.db.api import get_player_stats_new
        return get_player_stats_new(self.id)

    @property
    def primary_image_url(self):
        for image in self.images:
            if image.is_primary:
                return image.url
        return 'no-img.jpg'

    # TODO - Select for different teams (e.g. 1st and reserves for the same player)
    @property
    def league_appearances(self):
        return sum(1 for x in self.team_player_registrations[0].fixture_squads
                   if x.fixture.season_gameweek.season.division.competition.is_league
                   and x.fixture.season_gameweek.season.start_date >= datetime.strptime('2017-08-01',
                                                                                        '%Y-%m-%d').date())

    # TODO - Select for different teams (e.g. 1st and reserves for the same player)
    @property
    def cup_appearances(self):
        return sum(1 for x in self.team_player_registrations[0].fixture_squads
                   if x.fixture.season_gameweek.season.division.competition.is_cup
                   and x.fixture.season_gameweek.season.start_date >= datetime.strptime('2017-08-01',
                                                                                        '%Y-%m-%d').date())

    # TODO - Select for different teams (e.g. 1st and reserves for the same player)
    @property
    def friendly_appearances(self):
        return sum(1 for x in self.team_player_registrations[0].fixture_squads
                   if x.fixture.season_gameweek.season.division.competition.is_friendly
                   and (
                   x.fixture.season_gameweek.season.start_date >= datetime.strptime('2017-08-01', '%Y-%m-%d').date()))

    @property
    def total_season_appearances(self):
        return self.league_appearances + self.cup_appearances + self.friendly_appearances

    @property
    def total_season_competitive_appearances(self):
        return self.league_appearances + self.cup_appearances

    @property
    def league_goals(self):
        return sum(1 for x in self.goals if x.fixture.season_gameweek.season.is_current and
                   x.fixture.season_gameweek.season.division.competition.is_league)

    @property
    def cup_goals(self):
        return sum(1 for x in self.goals if x.fixture.season_gameweek.season.is_current and
                   x.fixture.season_gameweek.season.division.competition.is_cup)

    @property
    def friendly_goals(self):
        return sum(1 for x in self.goals if x.fixture.season_gameweek.season.is_current and
                   x.fixture.season_gameweek.season.division.competition.is_friendly)

    @property
    def total_season_goals(self):
        return self.league_goals + self.cup_goals + self.friendly_goals

    @property
    def total_season_competitive_goals(self):
        return self.league_goals + self.cup_goals

    @property
    def league_assists(self):
        return sum(1 for x in self.assists if x.goal.fixture.season_gameweek.season.is_current and
                   x.goal.fixture.season_gameweek.season.division.competition.is_league)

    @property
    def cup_assists(self):
        return sum(1 for x in self.assists if x.goal.fixture.season_gameweek.season.is_current and
                   x.goal.fixture.season_gameweek.season.division.competition.is_cup)

    @property
    def friendly_assists(self):
        return sum(1 for x in self.assists if x.goal.fixture.season_gameweek.season.is_current and
                   x.goal.fixture.season_gameweek.season.division.competition.is_friendly)

    @property
    def total_season_assists(self):
        return self.league_assists + self.cup_assists + self.friendly_assists

    @property
    def total_season_competitive_assists(self):
        return self.league_assists + self.cup_assists


class PlayerImage(Base):
    __tablename__ = 'player_image'

    id = Column(Integer, primary_key=True, autoincrement=True)
    player_id = Column(Integer, ForeignKey("player.id"))
    url = Column(Text, nullable=False)
    is_primary = Column(Boolean, nullable=False, default=0)
    status_id = Column(Integer, nullable=False, default=1)


class Club(Base):
    __tablename__ = 'club'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(Text)

    teams = relationship('Team')
    """ :type list[whfc.db.models.Team] """

    player_registrations = relationship('ClubPlayerRegistration')
    """ :type list[whfc.db.models.ClubPlayerRegistration] """

    def get_players(self):
        return [x.player for x in self.player_registrations]

    def add_team(self, team_name):
        team = Team()
        team.name = team_name
        team.club = self

        db.session.add(team)

    def add_player(self, player, registered_from=None, registered_to=None):
        registration = ClubPlayerRegistration()
        registration.player = player
        registration.registered_from = registered_from if registered_from else datetime.now()
        registration.registered_to = registered_to
        registration.club = self

        db.session.add(registration)


class ClubPlayerRegistration(Base):
    __tablename__ = 'club_player_registration'

    id = Column(Integer, primary_key=True, autoincrement=True)
    club_id = Column(Integer, ForeignKey("club.id"))
    player_id = Column(Integer, ForeignKey("player.id"))
    registered_from = Column(Date, nullable=False)
    registered_to = Column(Date, nullable=True)

    club = relationship('Club')
    """ :type whfc.db.models.Club """

    player = relationship('Player')
    """ :type whfc.db.models.Player """


class ClubTeamPlayerRegistration(Base):
    __tablename__ = 'club_team_player_registration'

    id = Column(Integer, primary_key=True, autoincrement=True)
    team_id = Column(Integer, ForeignKey("team.id"))
    player_id = Column(Integer, ForeignKey("player.id"))
    registered_from = Column(Date, nullable=False)
    registered_to = Column(Date, nullable=True)

    team = relationship('Team')
    """ :type whfc.db.models.Team """

    player = relationship('Player')
    """ :type whfc.db.models.Player """

    fixture_squads = relationship('FixtureSquadPlayer')
    """ :type list[whfc.db.models.FixtureSquadPlayer] """


class Team(Base):
    __tablename__ = 'team'

    id = Column(Integer, primary_key=True, autoincrement=True)
    club_id = Column(Integer, ForeignKey("club.id"))
    name = Column(Text, nullable=False)

    club = relationship('Club')
    """ :type whfc.db.models.Club """

    player_registrations = relationship('ClubTeamPlayerRegistration')
    """ :type list[whfc.db.models.ClubTeamPlayerRegistration] """

    @property
    def is_whfc(self):
        return self.club.id == 1

    def get_name(self, length):
        if self.is_whfc:
            return self.name
        return self.name[:length-2] + '..' if len(self.name) > length else self.name

    def get_players(self):
        return [x.player for x in self.player_registrations]

    def add_player(self, player):
        team_player = ClubTeamPlayerRegistration()
        team_player.player = player
        team_player.team = self

        db.session.add(team_player)

    def get_fixtures(self, played, fixtures_to_get):
        from whfc.db.api import get_fixtures_by_team_id
        fixtures = get_fixtures_by_team_id(self.id)
        if played:
            fixtures = [x for x in fixtures if x.result and not x.is_postponed]
        else:
            fixtures = [x for x in fixtures if x.result is None and not x.is_postponed]
        return fixtures[-fixtures_to_get:]


class Season(Base):
    __tablename__ = 'season'

    id = Column(Integer, primary_key=True, autoincrement=True)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=False)
    name = Column(Text, nullable=False)


class Competition(Base):
    __tablename__ = 'competition'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(Text)
    format_type_id = Column(Integer, ForeignKey("competition_format.id"))

    divisions = relationship('CompetitionDivision')
    """ :type list[whfc.db.models.CompetitionDivision] """

    @property
    def competition_name(self):
        if self.format_type_id == 1:
            return "League"
        return self.name

    @property
    def type(self):
        if self.format_type_id == 1:
            return "League"
        if self.format_type_id in [2, 3]:
            return "Cup"
        if self.format_type_id == 4:
            return "Friendly"
        return ""

    @property
    def is_league(self):
        return self.format_type_id == 1

    @property
    def is_cup(self):
        return self.format_type_id in (2, 3)

    @property
    def is_friendly(self):
        return self.format_type_id == 4


class CompetitionFormat(Base):
    __tablename__ = 'competition_format'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(Text)
    description = Column(Text)


class CompetitionDivision(Base):
    __tablename__ = 'competition_division'

    id = Column(Integer, primary_key=True, autoincrement=True)
    competition_id = Column(Integer, ForeignKey("competition.id"))
    name = Column(Text)
    level_order = Column(Integer)

    competition = relationship('Competition')
    """ :type whfc.db.models.Competition """

    seasons = relationship('CompetitionDivisionSeason')
    """ :type list[whfc.db.models.CompetitionDivisionSeason] """


class CompetitionDivisionSeason(Base):
    __tablename__ = 'competition_division_season'

    id = Column(Integer, primary_key=True, autoincrement=True)
    competition_division_id = Column(Integer, ForeignKey("competition_division.id"))
    start_date = Column(Date)
    end_date = Column(Date)

    division = relationship('CompetitionDivision')
    """ :type whfc.db.models.CompetitionDivision """

    season_teams = relationship('CompetitionDivisionSeasonTeam', backref='competition_division_season')
    """ :type list[whfc.db.models.CompetitionDivisionSeasonTeam] """
    gameweeks = relationship('CompetitionDivisionSeasonGameweek', backref='competition_division_season')
    """ :type list[whfc.db.models.CompetitionDivisionSeasonGameweek] """

    @property
    def fixtures(self):
        season_fixtures = []
        for g in sorted(self.gameweeks, key=lambda x: x.start_date):
            season_fixtures.extend(g.fixtures)
        return season_fixtures

    def add_team(self, team):
        season_team = CompetitionDivisionSeasonTeam()
        season_team.team = team
        season_team.season = self

        db.session.add(season_team)

    def add_gameweek(self, start_date):
        season_gameweek = CompetitionDivisionSeasonGameweek()
        season_gameweek.start_date = start_date
        season_gameweek.season = self

        db.session.add(season_gameweek)

    @property
    def total_games(self):
        return sum(i.total_games for i in self.gameweeks)

    @property
    def games_played(self):
        return sum(i.games_played for i in self.gameweeks)

    @property
    def total_goals(self):
        return sum(i.total_goals for i in self.gameweeks)

    @property
    def goals_per_game(self):
        if self.games_played == 0:
            return 0
        return round((self.total_goals / self.games_played), 2)

    @property
    def start_date_league_heading(self):
        if not self.start_date:
            return ''
        return datetime.strftime(self.start_date, "%d %b %Y")

    @property
    def end_date_league_heading(self):
        if not self.end_date:
            return ''
        return datetime.strftime(self.end_date, "%d %b %Y")

    @property
    def is_current(self):
        if self.end_date is None:
            return self.start_date <= datetime.date(datetime.today())
        return self.start_date <= datetime.date(datetime.today()) <= self.end_date

    def get_table(self):
        """
        Get all of the gameweeks in the competition season.
        For each gameweek, get each game.
        For each match, add three points to the winning team's total points, or 1 point each for a draw.
        Add each team's goals to their totals.
        :return:
        """

        teams = {}
        for season_team in self.season_teams:
            teams[season_team.team.id] = TeamLeagueData()
            teams[season_team.team.id].id = season_team.team.id
            teams[season_team.team.id].name = season_team.team.name

        for gameweek in self.gameweeks:
            for game in gameweek.fixtures:

                if not game.is_postponed and game.result:

                    teams[game.home_team.id].played += 1
                    teams[game.away_team.id].played += 1

                    result = game.result

                    teams[game.home_team.id].goals_for += result.home_goals
                    teams[game.home_team.id].goals_against += result.away_goals
                    teams[game.home_team.id].goal_difference += result.home_goals - result.away_goals

                    teams[game.away_team.id].goals_for += result.away_goals
                    teams[game.away_team.id].goals_against += result.home_goals
                    teams[game.away_team.id].goal_difference += result.away_goals - result.home_goals

                    if result.home_goals == result.away_goals:
                        teams[game.home_team.id].drawn += 1
                        teams[game.away_team.id].drawn += 1
                        teams[game.home_team.id].points += 1
                        teams[game.away_team.id].points += 1
                    else:
                        if result.home_goals > result.away_goals:
                            winning_team = game.home_team
                            losing_team = game.away_team
                        else:
                            winning_team = game.away_team
                            losing_team = game.home_team

                        teams[winning_team.id].won += 1
                        teams[winning_team.id].points += 3

                        teams[losing_team.id].lost += 1

        sorted_teams = sorted(
                teams.values(),
                key=lambda x: x.name, reverse=False)
        return sorted(
                sorted_teams,
                key=lambda x: (x.points, x.goal_difference, x.goals_for, x.goals_against), reverse=True)


class TeamLeagueData(object):
    name = ""
    played = 0
    won = 0
    drawn = 0
    lost = 0
    goals_for = 0
    goals_against = 0
    goal_difference = 0
    points = 0


class CompetitionDivisionSeasonTeam(Base):
    __tablename__ = 'competition_division_season_team'

    id = Column(Integer, primary_key=True, autoincrement=True)
    competition_division_season_id = Column(Integer, ForeignKey("competition_division_season.id"))
    team_id = Column(Integer, ForeignKey("team.id"))

    season = relationship('CompetitionDivisionSeason')
    """ :type whfc.db.models.CompetitionDivisionSeason """
    team = relationship('Team')
    """ :type whfc.db.models.Team """


class CompetitionDivisionSeasonGameweek(Base):
    __tablename__ = 'competition_division_season_gameweek'

    id = Column(Integer, primary_key=True, autoincrement=True)
    competition_division_season_id = Column(Integer, ForeignKey("competition_division_season.id"))
    start_date = Column(Date)

    season = relationship('CompetitionDivisionSeason')
    """ :type whfc.db.models.CompetitionDivisionSeason """
    fixtures = relationship('GameweekFixture', uselist=True, backref='competition_division_season_gameweek')
    """ :type whfc.db.models.GameweekFixture """

    @property
    def total_games(self):
        return sum(1 for i in self.fixtures if i.result_type_id != 5)

    @property
    def games_played(self):
        return sum(1 for i in self.fixtures if i.result)

    @property
    def total_goals(self):
        return sum(i.total_goals for i in self.fixtures)


class GameweekFixture(Base):
    __tablename__ = 'gameweek_fixture'

    id = Column(Integer, primary_key=True, autoincrement=True)
    gameweek_id = Column(Integer, ForeignKey("competition_division_season_gameweek.id"))
    kick_off = Column(DateTime)
    location_id = Column(Integer, ForeignKey("location.id"))
    home_team_id = Column(Integer, ForeignKey("team.id"))
    away_team_id = Column(Integer, ForeignKey("team.id"))
    result_type_id = Column(Integer, ForeignKey("result_type.id"))
    url = Column(Text)

    home_team = relationship('Team', foreign_keys=home_team_id)
    """ :type whfc.db.models.Team """
    away_team = relationship('Team', foreign_keys=away_team_id)
    """ :type whfc.db.models.Team """

    location = relationship('Location')
    """ :type whfc.db.models.Location """

    season_gameweek = relationship('CompetitionDivisionSeasonGameweek', uselist=False, backref='gameweek_fixture')
    """ :type whfc.db.models.CompetitionDivisionSeasonGameweek """
    result = relationship('GameweekFixtureResult', uselist=False, backref='gameweek_fixture')
    """ :type whfc.db.models.GameweekFixtureResult """
    details = relationship('GameweekFixtureDetails', uselist=False, backref='gameweek_fixture')
    """ :type whfc.db.models.GameweekFixtureDetails """
    mom = relationship('FixtureMoM', uselist=False, backref='fixture_mom')
    """ :type whfc.db.models.FixtureMoM """

    fixture_players = relationship('FixtureSquadPlayer')
    """ :type whfc.db.models.FixtureSquadPlayer """

    event_goals = relationship('FixtureEventGoal')
    """ :type whfc.db.models.FixtureEventGoal """
    event_substitutions = relationship('FixtureEventSubstitution')
    """ :type whfc.db.models.FixtureEventSubstitution """
    event_cards = relationship('FixtureEventCard')
    """ :type whfc.db.models.FixtureEventCard """
    event_injuries = relationship('FixtureEventInjury')
    """ :type whfc.db.models.FixtureEventInjury """
    event_misc = relationship('FixtureEventMisc')
    """ :type whfc.db.models.FixtureEventMisc """

    preview = relationship('FixturePreview', uselist=False)
    """ :type whfc.db.models.FixturePreview """
    report = relationship('FixtureReport', uselist=False)
    """ :type whfc.db.models.FixtureReport """

    images = relationship('FixtureImage')
    """ :type whfc.db.models.FixtureImage """

    @property
    def opponent(self):
        return self.away_team if self.home_team.is_whfc else self.home_team

    @property
    def game_type(self):
        return self.season_gameweek.season.division.competition.type

    @property
    def is_postponed(self):
        return self.result_type_id == 5  # Postponed fixtures

    @property
    def total_goals(self):
        return self.result.total_goals if self.result else 0

    @property
    def home_players(self):
        return [x for x in self.fixture_players if x.team.id == self.home_team.id]

    @property
    def away_players(self):
        return [x for x in self.fixture_players if x.team.id == self.away_team.id]

    @property
    def whfc_result_type(self):
        if not self.result:
            return ''
        if not self.home_team.is_whfc and not self.away_team.is_whfc:
            return ''
        if self.result.home_goals == self.result.away_goals:
            return 'draw'
        if self.home_team.is_whfc and self.result.home_goals > self.result.away_goals:
            return 'win'
        if self.away_team.is_whfc and self.result.away_goals > self.result.home_goals:
            return 'win'
        return 'loss'

    @property
    def primary_image_url(self):
        for image in self.images:
            if image.is_primary:
                return image.url
        return None

    def get_competition_name(self):
        return self.season_gameweek.season.division.competition.competition_name

    def get_fixture_player_by_id(self, fixture_player_id):
        for fixture_player in self.fixture_players:
            if fixture_player.id == fixture_player_id:
                return fixture_player
        return None

    def get_fixture_player_by_player_id(self, player_id):
        for fixture_player in self.fixture_players:
            if fixture_player.player.id == player_id:
                return fixture_player
        return None

    def add_player(self, team_id, player_id, player_started):
        player = FixtureSquadPlayer()
        player.team_id = team_id
        player.team_player_id = player_id
        player.started = player_started

        self.fixture_players.append(player)

    def add_goal(self, team_id, player_id, minute, type_id, is_own_goal):
        goal = FixtureEventGoal()
        goal.team_id = team_id
        goal.player_id = player_id
        goal.minute = minute
        goal.goal_type_id = type_id
        goal.is_own_goal = is_own_goal

        self.event_goals.append(goal)

    def add_card(self, team_id, player_id, minute, type_id):
        card = FixtureEventCard()
        card.team_id = team_id
        card.player_id = player_id
        card.minute = minute
        card.card_type_id = type_id

        self.event_cards.append(card)

    def add_injury(self, team_id, player_id, minute, injury_type):
        injury = FixtureEventInjury()
        injury.team_id = team_id
        injury.player_id = player_id
        injury.minute = minute
        injury.injury_type = injury_type

        self.event_injuries.append(injury)

    def add_substitution(self, team_id, player_off_id, player_on_id, minute):
        sub = FixtureEventSubstitution()
        sub.team_id = team_id
        sub.player_off_id = player_off_id
        sub.player_on_id = player_on_id
        sub.minute = minute

        self.event_substitutions.append(sub)

    def set_mom(self, team_id, player_id):
        mom = FixtureMoM()
        mom.team_id = team_id
        mom.player_id = player_id

        self.mom = mom


class GameweekFixtureDetails(Base):
    __tablename__ = 'gameweek_fixture_details'

    id = Column(Integer, primary_key=True, autoincrement=True)
    fixture_id = Column(Integer, ForeignKey("gameweek_fixture.id"))
    url = Column(Text)
    report = Column(Text)
    album_url = Column(Text)
    h1 = Column(Text)
    short_desc = Column(Text)

    fixture = relationship('GameweekFixture', uselist=False, backref='gameweek_fixture_details')
    """ :type whfc.db.models.GameweekFixture """


class GameweekFixtureResult(Base):
    __tablename__ = 'gameweek_fixture_result'

    id = Column(Integer, primary_key=True, autoincrement=True)
    gameweek_fixture_id = Column(Integer, ForeignKey("gameweek_fixture.id"))
    home_goals = Column(Integer)
    away_goals = Column(Integer)

    fixture = relationship('GameweekFixture', uselist=False, backref='gameweek_fixture_result')
    """ :type whfc.db.models.GameweekFixture """

    def get_result_colour(self, team_id):
        if self.fixture.home_team_id == team_id:
            if self.home_goals > self.away_goals:
                return 'win'
            if self.home_goals < self.away_goals:
                return 'loss'
            return 'draw'
        if self.fixture.away_team_id == team_id:
            if self.away_goals > self.home_goals:
                return 'win'
            if self.away_goals < self.home_goals:
                return 'loss'
            return 'draw'

    @property
    def is_away_win(self):
        return self.home_goals > self.away_goals

    @property
    def is_draw_win(self):
        return self.home_goals > self.away_goals

    @property
    def total_goals(self):
        return self.home_goals + self.away_goals


class FixtureEventGoal(Base):
    __tablename__ = 'fixture_event_goal'

    id = Column(Integer, primary_key=True, autoincrement=True)
    fixture_id = Column(Integer, ForeignKey("gameweek_fixture.id"))
    team_id = Column(Integer, ForeignKey("team.id"), nullable=False)
    player_id = Column(Integer, ForeignKey("player.id"), nullable=True)
    minute = Column(Integer, nullable=True)
    goal_type_id = Column(Integer, ForeignKey("goal_type.id"), nullable=True)
    is_own_goal = Column(Boolean, nullable=False, default=0)

    fixture = relationship('GameweekFixture')
    """ :type whfc.db.models.GameweekFixture """
    team = relationship('Team')
    """ :type whfc.db.models.Team """
    player = relationship('Player')
    """ :type whfc.db.models.Player """
    type = relationship('GoalType')
    """ :type whfc.db.models.GoalType """

    assist = relationship('FixtureEventGoalAssist', uselist=False)
    """ :type whfc.db.models.FixtureEventGoalAssist """

    def add_assist(self, player_id):
        new_assist = FixtureEventGoalAssist()
        new_assist.player_id = player_id

        self.assist = new_assist


class FixtureEventGoalAssist(Base):
    __tablename__ = 'fixture_event_goal_assist'

    id = Column(Integer, primary_key=True, autoincrement=True)
    player_id = Column(Integer, ForeignKey("player.id"), nullable=True)
    goal_id = Column(Integer, ForeignKey("fixture_event_goal.id"), nullable=True)

    player = relationship('Player')
    """ :type whfc.db.models.Player """
    goal = relationship('FixtureEventGoal')
    """ :type whfc.db.models.FixtureEventGoal """


class FixtureEventSubstitution(Base):
    __tablename__ = 'fixture_event_substitution'

    id = Column(Integer, primary_key=True, autoincrement=True)
    fixture_id = Column(Integer, ForeignKey("gameweek_fixture.id"))
    team_id = Column(Integer, ForeignKey("team.id"), nullable=False)
    minute = Column(Integer, nullable=False)
    player_off_id = Column(Integer, ForeignKey("player.id"), nullable=True)
    player_on_id = Column(Integer, ForeignKey("player.id"), nullable=True)

    fixture = relationship('GameweekFixture')
    """ :type whfc.db.models.GameweekFixture """
    team = relationship('Team')
    """ :type whfc.db.models.Team """
    player_off = relationship('Player', foreign_keys=player_off_id)
    """ :type whfc.db.models.Player """
    player_on = relationship('Player', foreign_keys=player_on_id)
    """ :type whfc.db.models.Player """


class FixtureEventCard(Base):
    __tablename__ = 'fixture_event_card'

    id = Column(Integer, primary_key=True, autoincrement=True)
    fixture_id = Column(Integer, ForeignKey("gameweek_fixture.id"))
    team_id = Column(Integer, ForeignKey("team.id"), nullable=False)
    player_id = Column(Integer, ForeignKey("player.id"), nullable=True)
    minute = Column(Integer, nullable=True)
    card_type_id = Column(Integer, nullable=False)

    fixture = relationship('GameweekFixture')
    """ :type whfc.db.models.GameweekFixture """
    team = relationship('Team')
    """ :type whfc.db.models.Team """
    player = relationship('Player')
    """ :type whfc.db.models.Player """

    def __str__(self):
        return 'Yellow' if self.card_type_id == 1 else 'Red'


class FixtureEventInjury(Base):
    __tablename__ = 'fixture_event_injury'

    id = Column(Integer, primary_key=True, autoincrement=True)
    fixture_id = Column(Integer, ForeignKey("gameweek_fixture.id"))
    team_id = Column(Integer, ForeignKey("team.id"), nullable=False)
    player_id = Column(Integer, ForeignKey("player.id"), nullable=True)
    minute = Column(Integer, nullable=False)
    injury_type = Column(Text, nullable=True)

    fixture = relationship('GameweekFixture')
    """ :type whfc.db.models.GameweekFixture """
    team = relationship('Team')
    """ :type whfc.db.models.Team """
    player = relationship('Player')
    """ :type whfc.db.models.Player """


class FixtureEventMisc(Base):
    __tablename__ = 'fixture_event_misc'

    id = Column(Integer, primary_key=True, autoincrement=True)
    fixture_id = Column(Integer, ForeignKey("gameweek_fixture.id"))
    event_type_id = Column(Integer, ForeignKey("event_type.id"))
    minute = Column(Integer, nullable=True)
    team_id = Column(Integer, ForeignKey("team.id"), nullable=True)
    player_id = Column(Integer, ForeignKey("player.id"), nullable=True)
    event_details = Column(Text, nullable=True)

    team = relationship('Team')
    """ :type whfc.db.models.Team """

    player = relationship('Player')
    """ :type whfc.db.models.Player """

    type = relationship('EventType')
    """ :type whfc.db.models.EventType """


class FixtureMoM(Base):
    __tablename__ = 'fixture_mom'

    id = Column(Integer, primary_key=True, autoincrement=True)
    fixture_id = Column(Integer, ForeignKey("gameweek_fixture.id"))
    team_id = Column(Integer, ForeignKey("team.id"), nullable=False)
    player_id = Column(Integer, ForeignKey("player.id"), nullable=False)

    fixture = relationship('GameweekFixture')
    """ :type whfc.db.models.GameweekFixture """
    team = relationship('Team')
    """ :type whfc.db.models.Team """
    player = relationship('Player')
    """ :type whfc.db.models.Player """


class FixturePreview(Base):
    __tablename__ = 'fixture_preview'

    id = Column(Integer, primary_key=True, autoincrement=True)
    fixture_id = Column(Integer, ForeignKey("gameweek_fixture.id"))


class FixtureReport(Base):

    __tablename__ = 'fixture_report'

    id = Column(Integer, primary_key=True, autoincrement=True)
    fixture_id = Column(Integer, ForeignKey("gameweek_fixture.id"))
    url = Column(Text, nullable=True)
    title = Column(Text, nullable=True)
    content = Column(Text, nullable=True)


class FixtureImage(Base):
    __tablename__ = 'fixture_image'

    id = Column(Integer, primary_key=True, autoincrement=True)
    fixture_id = Column(Integer, ForeignKey("gameweek_fixture.id"))
    url = Column(Text, nullable=True)
    is_primary = Column(Boolean, nullable=False, default=0)


class GoalType(Base):
    __tablename__ = 'goal_type'

    id = Column(Integer, primary_key=True, autoincrement=True)
    type = Column(Text, nullable=False)


class EventType(Base):
    __tablename__ = 'event_type'

    id = Column(Integer, primary_key=True, autoincrement=True)
    type = Column(Text, nullable=False)


class Location(Base):
    __tablename__ = 'location'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(Text)


class ResultType(Base):
    __tablename__ = 'result_type'

    id = Column(Integer, primary_key=True, autoincrement=True)
    type = Column(Text)
    description = Column(Text)


class TrainingSession(Base):
    __tablename__ = 'training_session'

    id = Column(Integer, primary_key=True, autoincrement=True)
    location_id = Column(Integer, ForeignKey("location.id"))
    date = Column(Date)
    description = Column(Text)


class PlayerDebit(Base):
    __tablename__ = 'player_debit'

    id = Column(Integer, primary_key=True, autoincrement=True)
    player_id = Column(Integer, ForeignKey("player.id"))
    reason_id = Column(Integer, ForeignKey("map_player_debit_reason.id"))
    date_owed = Column(Date)
    amount = Column(Numeric(8, 2))

    reason = relationship('PlayerDebitReason', backref='player_debit')
    """ :type whfc.db.models.PlayerDebitReason """


class PlayerDebitReason(Base):
    __tablename__ = 'map_player_debit_reason'

    id = Column(Integer, primary_key=True, autoincrement=True)
    reason = Column(Text)


class PlayerCredit(Base):
    __tablename__ = 'player_credit'

    id = Column(Integer, primary_key=True, autoincrement=True)
    player_id = Column(Integer, ForeignKey("player.id"))
    amount = Column(Numeric(8, 2))
    date_paid = Column(Date)


class KitWash(Base):
    __tablename__ = 'kit_wash'

    id = Column(Integer, primary_key=True, autoincrement=True)
    fixture_id = Column(Integer, ForeignKey("gameweek_fixture.id"))
    player_id = Column(Integer, ForeignKey("club_team_player_registration.id"))


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(Text, nullable=False)
    password = Column(Text, nullable=False)
    type_id = Column(Integer, nullable=False)
    status_id = Column(Integer, nullable=False, default=1)
    date_created = Column(Date, nullable=False, default=datetime.now())
    date_updated = Column(Date, nullable=True)

    def check_password(self, password):
        return bcrypt.checkpw(password.encode('utf-8'), self.password.encode('utf-8'))

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    @property
    def is_authenticated(self):
        return True

    @property
    def type(self):
        if self.role_type_id == 1:
            return "Team Admin"
        if self.role_type_id == 2:
            return "Team User"

    @property
    def status(self):
        if self.status_id == 1:
            return "Pending"
        if self.status_id == 2:
            return "Active"
        if self.status_id == 3:
            return "Deleted"
        if self.status_id == 4:
            return "Banned"


class FixtureSquadPlayer(Base):
    __tablename__ = 'gameweek_fixture_squad_player'

    id = Column(Integer, primary_key=True, autoincrement=True)
    fixture_id = Column(Integer, ForeignKey("gameweek_fixture.id"), nullable=False)
    team_id = Column(Integer, ForeignKey("team.id"), nullable=False)
    team_player_id = Column(Integer, ForeignKey("club_team_player_registration.id"), nullable=False)
    started = Column(Boolean, nullable=False)
    position_id = Column(Integer, ForeignKey("position.id"))

    fixture = relationship('GameweekFixture')
    """ :type whfc.db.models.GameweekFixture """
    team = relationship('Team')
    """ :type whfc.db.models.Team """
    team_player_registration = relationship('ClubTeamPlayerRegistration')
    """ :type whfc.db.models.ClubTeamPlayerRegistration """
    position = relationship('Position')
    """ :type whfc.db.models.Position """

    @property
    def player(self):
        return self.team_player_registration.player

    @property
    def is_goalkeeper(self):
        return self.position.is_goalkeeper if self.position else False

    @property
    def is_defender(self):
        return self.position.is_defender if self.position else False

    @property
    def is_midfielder(self):
        return self.position.is_midfielder if self.position else False

    @property
    def is_attacker(self):
        return self.position.is_attacker if self.position else False

    def pitch_order_rank(self):
        return self.position.pitch_order_rank if self.position else -1


class MapPositionType(Base):
    __tablename__ = 'map_position_type'

    id = Column(Integer, primary_key=True, autoincrement=True)
    type = Column(Text, nullable=False)

    @property
    def is_goalkeeper(self):
        return self.type == 'Goalkeeper'

    @property
    def is_defender(self):
        return self.type == 'Defense'

    @property
    def is_midfielder(self):
        return self.type == 'Midfield'

    @property
    def is_attacker(self):
        return self.type == 'Attack'


class MapPosition(Base):
    __tablename__ = 'map_position'

    id = Column(Integer, primary_key=True, autoincrement=True)
    position_type_id = Column(Integer, ForeignKey("map_position_type.id"), nullable=False)
    position = Column(Text, nullable=False)

    position_type = relationship('MapPositionType')
    """ :type whfc.db.models.MapPositionType """

    @property
    def is_goalkeeper(self):
        return self.position_type.is_goalkeeper

    @property
    def is_central_defender(self):
        return self.id == 2

    @property
    def is_defender(self):
        return self.position_type.is_defender

    @property
    def is_midfielder(self):
        return self.position_type.is_midfielder

    @property
    def is_attacker(self):
        return self.position_type.is_attacker


class MapPositionSide(Base):
    __tablename__ = 'map_position_side'

    id = Column(Integer, primary_key=True, autoincrement=True)
    side = Column(Text, nullable=False)

    @property
    def is_center(self):
        return self.side == 'Center'

    @property
    def is_right(self):
        return self.side == 'Right'

    @property
    def is_left(self):
        return self.side == 'Left'


class Position(Base):
    __tablename__ = 'position'

    id = Column(Integer, primary_key=True, autoincrement=True)
    position_id = Column(Integer, ForeignKey("map_position.id"), nullable=False)
    side_id = Column(Integer, ForeignKey("map_position_side.id"), nullable=False)
    pitch_order_rank = Column(Integer, nullable=False)

    map_position = relationship('MapPosition')
    """ :type whfc.db.models.MapPosition """

    map_position_side = relationship('MapPositionSide')
    """ :type whfc.db.models.MapPositionSide """

    def __str__(self):
        if self.map_position.is_goalkeeper:
            return self.map_position.position
        if self.map_position.is_central_defender:
            return self.map_position.position
        return self.map_position_side.side + ' ' + self.map_position.position

    @property
    def is_goalkeeper(self):
        return self.map_position.is_goalkeeper

    @property
    def is_defender(self):
        return self.map_position.is_defender

    @property
    def is_midfielder(self):
        return self.map_position.is_midfielder

    @property
    def is_attacker(self):
        return self.map_position.is_attacker

    @property
    def is_center(self):
        return self.map_position_side.is_center

    @property
    def is_right(self):
        return self.map_position_side.is_right

    @property
    def is_left(self):
        return self.map_position_side.is_left

    @property
    def for_select(self):
        return self.map_position.position + ' (' + self.map_position_side.side + ')'
