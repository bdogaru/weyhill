import datetime
from sqlalchemy import null, or_

from whfc.create_app import db
from whfc.db.models import Players, Competition, CompetitionDivision, CompetitionDivisionSeason, \
    CompetitionDivisionSeasonGameweek, GameweekFixture, ClubTeamPlayerRegistration, FixtureEventGoal, Position


def get_all_players():
    return db.session.query(Players) \
        .filter(Players.status == 1) \
        .all()


def get_player(first_name, last_name):
    return db.session.query(Players) \
        .filter(Players.first_name == first_name) \
        .filter(Players.last_name == last_name) \
        .one()


def get_player_by_id_new(player_id):
    return db.session.query(ClubTeamPlayerRegistration) \
        .filter(ClubTeamPlayerRegistration.id == player_id) \
        .one()


def check_member(username):
    from whfc.db.models import StaffMembers
    return db.session.query(StaffMembers) \
        .filter(StaffMembers.username == username).one()


def get_player_stats(player_id):
    from whfc.db.models import PlayerStats
    return db.session.query(PlayerStats).filter(PlayerStats.player_id == player_id).one()


def get_player_stats_new(new_player_id):
    from whfc.db.models import PlayerStats
    return db.session.query(PlayerStats).filter(PlayerStats.new_player_id == new_player_id).one()


def get_fixture_by_url(url):
    from whfc.db.models import Fixtures
    return db.session.query(Fixtures).filter(Fixtures.url == url).one()


def get_all_games():
    from whfc.db.models import Fixtures
    return db.session.query(Fixtures) \
        .filter(Fixtures.game == 1) \
        .all()


def get_last_fixture():
    from whfc.db.models import Fixtures
    return db.session.query(Fixtures) \
        .filter(Fixtures.game == 1) \
        .filter(Fixtures.score != null) \
        .order_by(Fixtures.id.desc()) \
        .first()


# New methods


def get_all_players_new():
    from whfc.db.models import Player
    return db.session.query(Player) \
        .all()


def get_all_money_owed(player_id):
    from whfc.db.models import PlayerDebit
    return db.session.queury(PlayerDebit) \
        .filter(PlayerDebit.player_id == player_id) \
        .all()


def get_all_money_paid(player_id):
    from whfc.db.models import PlayerCredit
    return db.session.queury(PlayerCredit) \
        .filter(PlayerCredit.player_id == player_id) \
        .all()


def get_fixtures():
    return db.session.query(GameweekFixture) \
        .order_by(GameweekFixture.kick_off.asc()) \
        .all()


def get_season_fixtures(season_id):
    return db.session.query(GameweekFixture) \
        .join(CompetitionDivisionSeasonGameweek) \
        .join(CompetitionDivisionSeason) \
        .filter(CompetitionDivisionSeason.id == season_id) \
        .order_by(GameweekFixture.kick_off.asc()) \
        .all()


def get_fixtures_by_team_id(team_id):
    return db.session.query(GameweekFixture) \
        .order_by(GameweekFixture.kick_off.asc()) \
        .filter(or_(GameweekFixture.home_team_id == team_id, GameweekFixture.away_team_id == team_id)) \
        .all()


def get_division_season_fixtures_by_team_id(division_season_id, team_id):
    return db.session.query(GameweekFixture) \
        .join(CompetitionDivisionSeasonGameweek) \
        .join(CompetitionDivisionSeason) \
        .order_by(GameweekFixture.kick_off.asc()) \
        .filter(CompetitionDivisionSeason.id == division_season_id) \
        .filter(or_(GameweekFixture.home_team_id == team_id, GameweekFixture.away_team_id == team_id)) \
        .all()


def get_competition_season_gameweek_by_id(competition_season_gameweek_id):
    return db.session.query(CompetitionDivisionSeasonGameweek) \
        .filter(CompetitionDivisionSeasonGameweek.id == competition_season_gameweek_id) \
        .first()


def get_competition_by_id(competition_id):
    return db.session.query(Competition) \
        .filter(Competition.id == competition_id) \
        .one()


def get_competition_division_by_id(competition_division_id):
    return db.session.query(CompetitionDivision) \
        .filter(CompetitionDivision.id == competition_division_id) \
        .one()


def get_fixture_by_id(fixture_id):
    from whfc.db.models import GameweekFixture
    return db.session.query(GameweekFixture) \
        .filter(GameweekFixture.id == fixture_id) \
        .one()


def get_all_competitions():
    from whfc.db.models import Competition
    return db.session.query(Competition).all()


def get_locations_all():
    from whfc.db.models import Location
    return db.session.query(Location) \
        .all()


def get_club_by_id(club_id):
    from whfc.db.models import Club
    return db.session.query(Club) \
        .filter(Club.id == club_id) \
        .one()


def get_teams_all():
    from whfc.db.models import Team
    return db.session.query(Team).all()


def get_team_by_id(team_id):
    from whfc.db.models import Team
    return db.session.query(Team).filter(Team.id == team_id).one()


def get_club_team_by_id(team_id, club_id=None):
    from whfc.db.models import Team
    query = db.session.query(Team) \
        .filter(Team.id == team_id)
    if club_id:
        query \
            .filter(Team.club_id == club_id)
    return query.one()


def get_club_players(club_id):
    from whfc.db.models import Player, ClubPlayerRegistration

    query = db.session.query(Player) \
        .join(ClubPlayerRegistration) \
        .filter(Player.status_id == 1) \
        .filter(ClubPlayerRegistration.club_id == club_id) \
        .filter(ClubPlayerRegistration.registered_from <= datetime.datetime.now()) \
        .filter(ClubPlayerRegistration.registered_to >= datetime.datetime.now())

    return query.all()


def get_club_players_by_registration_dates(club_id, registered_from=None, registered_to=None):
    from whfc.db.models import Player, ClubPlayerRegistration

    query = db.session.query(ClubPlayerRegistration) \
        .join(Player) \
        .filter(ClubPlayerRegistration.club_id == club_id)

    if registered_from and registered_to:
        query \
            .filter(ClubPlayerRegistration.registered_from <= registered_from) \
            .filter(ClubPlayerRegistration.registered_to >= registered_to)
    elif registered_from:
        query \
            .filter(ClubPlayerRegistration.registered_from >= registered_from)
    elif registered_to:
        query \
            .filter(ClubPlayerRegistration.registered_to <= registered_to)

    return query.all()


def get_club_team_players_by_date(team_id, fixture_date):

    return db.session.query(ClubTeamPlayerRegistration) \
        .filter(ClubTeamPlayerRegistration.team_id == team_id) \
        .filter(ClubTeamPlayerRegistration.registered_from <= fixture_date) \
        .filter(ClubTeamPlayerRegistration.registered_to >= fixture_date) \
        .all()


def get_club_team_players_by_dates(team_id, date_from, date_to):

    return db.session.query(ClubTeamPlayerRegistration) \
        .filter(ClubTeamPlayerRegistration.team_id == team_id) \
        .filter(ClubTeamPlayerRegistration.registered_from >= date_from) \
        .filter(ClubTeamPlayerRegistration.registered_to <= date_to) \
        .all()


def get_club_player_by_id(player_id, club_id=None):
    from whfc.db.models import ClubPlayerRegistration, Player
    query = db.session.query(Player) \
        .join(ClubTeamPlayerRegistration) \
        .filter(Player.id == player_id)
    if club_id:
        query.filter(ClubPlayerRegistration.club_id == club_id)
    return query.one()


def get_team_player_by_id(team_player_id):
    return db.session.query(ClubTeamPlayerRegistration) \
        .filter(ClubTeamPlayerRegistration.id == team_player_id) \
        .one()


def get_team_player_by_player_id_team_id(player_id, team_id):
    return db.session.query(ClubTeamPlayerRegistration) \
        .filter(ClubTeamPlayerRegistration.team_id == player_id) \
        .filter(ClubTeamPlayerRegistration.team_id == team_id) \
        .one()


def get_teams_by_season_division_id(division_id):
    from whfc.db.models import CompetitionDivisionSeasonTeam
    return db.session.query(CompetitionDivisionSeasonTeam) \
        .filter(CompetitionDivisionSeasonTeam.competition_division_season_id == division_id) \
        .all()


def get_division_season_by_id(division_season_id):
    from whfc.db.models import CompetitionDivisionSeason
    return db.session.query(CompetitionDivisionSeason) \
        .filter(CompetitionDivisionSeason.id == division_season_id) \
        .one()


def get_division_season_gameweek_by_id(season_id, gameweek_id):
    from whfc.db.models import CompetitionDivisionSeasonGameweek
    return db.session.query(CompetitionDivisionSeasonGameweek) \
        .filter(CompetitionDivisionSeasonGameweek.id == gameweek_id) \
        .filter(CompetitionDivisionSeasonGameweek.competition_division_season_id == season_id) \
        .one()


def get_gameweek_fixture_by_id(gameweek_id, fixture_id):
    from whfc.db.models import GameweekFixture
    return db.session.query(GameweekFixture) \
        .filter(GameweekFixture.id == fixture_id) \
        .filter(GameweekFixture.gameweek_id == gameweek_id) \
        .one()


def get_debit_reasons_all():
    from whfc.db.models import PlayerDebitReason
    return db.session.query(PlayerDebitReason) \
        .all()


def get_previous_fixtures(team_id, fixtures_to_get):
    """
    Get played fixtures for the specified team. This excludes postponed games
    :param team_id: The ID of the team to return the fixtures for
    :type fixtures_to_get: int
    :param fixtures_to_get: The maximum number of previous fixtures to return
    :type fixtures_to_get: int
    :return: whfc.db.models.GameweekFixture
    """
    fixtures = get_fixtures_by_team_id(team_id)
    previous_fixtures = [x for x in fixtures if x.result and not x.is_postponed]
    return sorted(previous_fixtures[-fixtures_to_get:], key=lambda x: x.kick_off)


def get_next_fixtures(team_id, fixtures_to_get):
    """
    Get unplayed fixtures for the specified team. This excludes postponed games
    :param team_id: The ID of the team to return the fixtures for
    :type fixtures_to_get: int
    :param fixtures_to_get: The maximum number of unplayed fixtures to return
    :type fixtures_to_get: int
    :return: whfc.db.models.GameweekFixture
    """
    fixtures = get_fixtures_by_team_id(team_id)
    unplayed_fixtures = [x for x in fixtures if not x.result and not x.is_postponed]
    return unplayed_fixtures[:fixtures_to_get]


def get_all_staff_members():
    from whfc.db.models import StaffMembers
    return db.session.query(StaffMembers).all()


def get_all_users():
    from whfc.db.models import User
    return db.session.query(User).all()


def get_all_clubs():
    from whfc.db.models import Club
    return db.session.query(Club).all()


def get_player_by_id(player_id):
    from whfc.db.models import Player
    return db.session.query(Player) \
        .filter(Player.id == player_id) \
        .one()


def get_staff_by_url(staff_url):
    from whfc.db.models import StaffMembers
    return db.session.query(StaffMembers) \
        .filter(StaffMembers.url == staff_url) \
        .one()


def get_all_goal_types():
    from whfc.db.models import GoalType
    return db.session.query(GoalType).all()


def get_fixture_players(fixture_id, team_id):
    from whfc.db.models import FixtureSquadPlayer
    return db.session.query(FixtureSquadPlayer) \
        .filter(FixtureSquadPlayer.fixture_id == fixture_id) \
        .filter(FixtureSquadPlayer.team_id == team_id) \
        .all()


def get_goal_by_id(goal_id):
    return db.session.query(FixtureEventGoal) \
        .filter(FixtureEventGoal.id == goal_id) \
        .one()


def all_positions():
    return db.session.query(Position) \
        .all()


def get_position_by_id(position_id):
    return db.session.query(Position) \
        .filter(Position.id == position_id) \
        .one()
