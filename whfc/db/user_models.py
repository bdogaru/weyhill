class AnonymousUser(object):

    def __init__(self):
        self.id = None

    @property
    def is_authenticated(self):
        return False

    @property
    def is_active(self):
        return False

    @property
    def is_anonymous(self):
        return True

    def get_id(self):
        return self.id
