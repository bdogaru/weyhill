$(function(){
    $(document).on('click', '.tabs-heads > li', function(){
        var tab = $(this).data('tab');
        var related = $(this).parents('.tabs-wrap').find('.tabs-content > li');
        $(this).addClass('active').siblings('li').removeClass('active');
        related.each(function(){
            if ($(this).hasClass(tab)) $(this).show();
            else $(this).hide();
        })
    });

    $('.tabs-wrap').each(function(){
       var tab = $(this).find('.tabs-heads').find('.active').data('tab');
        var related = $(this).find('.tabs-content > li');
        related.each(function(){
            if ($(this).hasClass(tab)) $(this).show();
            else $(this).hide();
        })
    });

    $(document).on('mouseenter', 'nav a', function(){
        $(this).addClass('active');
        $(this).parent().siblings('li').find('a').addClass('inactive');
    });

    $(document).on('mouseleave', 'nav a', function(){
        $(this).removeClass('active');
        $(this).parents('nav').find('a').removeClass('inactive');
    });

    $(document).on('click', '.mobile-trigger', function(){
        var menu = $('nav');
        var $this = $(this);
        if (menu.is(':visible')) {
            menu.slideUp(function(){
                $('header').removeClass('active');
                $this.find('i').removeClass('icon-remove').addClass('icon-menu');
                $this.removeClass('active');
            });
        } else {
            menu.slideDown();
            $this.find('i').addClass('icon-remove').removeClass('icon-menu');
            $this.addClass('active');
            $('header').addClass('active');
        }
    });

    $(document).on('change', '.g-list select', function(){
        var img = $(this).siblings('a').find('img');
        var colour = $(this).val();
        img.prop('src', img.prop('src').slice(0, -5) + colour + '.jpg');
        img.parent().prop('href', img.prop('src').slice(0, -5) + colour + '.jpg');
    });

});