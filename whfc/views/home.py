from urlparse import urlparse, urlunparse

from decimal import Decimal
from flask import render_template, redirect, request, session, url_for
from flask_login import login_required, current_user, login_user
from oslo_config import cfg
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound

from whfc.create_app import app
from whfc.db.api import check_member, get_player_stats, get_previous_fixtures, get_next_fixtures, \
    get_division_season_by_id, get_all_players, get_season_fixtures, get_division_season_fixtures_by_team_id, \
    get_locations_all, get_fixture_by_id, get_all_competitions, get_club_players, get_player_by_id, \
    get_all_staff_members, get_club_team_players_by_date, get_club_team_players_by_dates

import pandas as pd


@app.route('/')
def home():
    return render_template('base.html')
