from sqlalchemy.orm.exc import NoResultFound

from whfc.create_app import login_manager
from whfc.db.api import check_member


@login_manager.user_loader
def load_user(username):
    """
    Flask-Login method to load the user who is currently logged in
    :param username:
    """
    try:

        # Attempt to load the user from the database
        member = check_member(username)

        return member

    except NoResultFound:

        # If we couldn't load the user, return None as required by Flask-Login
        return None
