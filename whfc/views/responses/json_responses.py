from flask import Response, json


def json_response(response, status_code=200):
    return Response(json.dumps(response), mimetype='application/json', status=status_code)


def json_success(response=None):
    if response is None:
        response = {u'result': True}
    else:
        response[u'result'] = True

    return json_response(response)


def json_error(error=None, status_code=200):
    response = {u'result': False}
    if error is not None:
        response[u'error'] = error

    return json_response(response, status_code=status_code)


def json_failure(error=None, status_code=400):
    response = {u'result': False}
    if error is not None:
        response[u'error'] = error

    return json_response(response, status_code)